package com.desafio.scc4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan(basePackages = "com.desafio.scc4.model")
@ComponentScan(basePackages = {"com.*"})
@EnableJpaRepositories(basePackages = {"com.desafio.scc4.repository"})
@EnableTransactionManagement
public class DesafioScc4Application {

	public static void main(String[] args) {
		SpringApplication.run(DesafioScc4Application.class, args);
	}

}
