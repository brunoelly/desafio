package com.desafio.scc4.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desafio.scc4.model.ListaReversaModel;

@Repository
public interface ListaReversaRepository extends CrudRepository<ListaReversaModel, Long> {

}
