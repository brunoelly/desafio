package com.desafio.rest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.desafio.rest.model.Destinatario;
import com.desafio.rest.repository.DestinatarioRepository;

@RestController
@RequestMapping("/destinatarios")
public class DestinatarioController {

	@Autowired
	private DestinatarioRepository DestinatarioRepository;

	@GetMapping
	public List findAll() {
		return (List) DestinatarioRepository.findAll();
	}

	@GetMapping(path = { "/{id}" })
	public ResponseEntity findById(@PathVariable long id) {
		return DestinatarioRepository.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Destinatario criaNovo(@RequestBody Destinatario destinatario) {
		return DestinatarioRepository.save(destinatario);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity update(@PathVariable("id") long id, @RequestBody Destinatario destinatario) {
		return DestinatarioRepository.findById(id).map(record -> {
			record.setNome(destinatario.getNome());
			record.setCpf(destinatario.getCpf());
			record.setEmail(destinatario.getEmail());
			record.setEndereço(destinatario.getEndereço());
			record.setTelefone(destinatario.getTelefone());
			Destinatario updated = DestinatarioRepository.save(record);
			return ResponseEntity.ok().body(updated);
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping(path ={"/{id}"})
	public ResponseEntity<?> delete(@PathVariable long id) {
	   return DestinatarioRepository.findById(id)
	           .map(record -> {
	               DestinatarioRepository.deleteById(id);
	               return ResponseEntity.ok().build();
	           }).orElse(ResponseEntity.notFound().build());
	}
	
}