package com.desafio.rest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desafio.rest.model.Destinatario;

@Repository
public interface DestinatarioRepository extends CrudRepository<Destinatario, Long> {

}
