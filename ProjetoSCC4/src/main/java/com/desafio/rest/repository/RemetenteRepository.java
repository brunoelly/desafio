package com.desafio.rest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desafio.rest.model.Remetente;

@Repository
public interface RemetenteRepository extends CrudRepository<Remetente, Long>{

}
