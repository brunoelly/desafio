# desafio
Este repositório contém a implementação do desafio para Dev Java SCC4

No Diretório DesafioSCC4, encontra-se a implementação da parte 1 do exercício proposto
No diretório ProjetoSCC4, encontra-se a implementação da parte 2.

Algumas features não foram implementadas:

1. view com Angular 
2. mascaras para cpf e telefone - poderiam ter sido implementadas com Regex ou diretamente na view.
Creio que não seria uma boa prática implementar direto na API, portanto, como não tenho os conhecimentos de Angular, essa feature
não entra nesta primeira versão. 
Havia a possibildade de usar a classe MaskFormatter no caso de JSF.
3. A calculadora não faz algumas validações necessárias, mas funciona com o básico.
